import sqlite3
class Table:
    
    def __init__(self,conn,nom_table):
        self.nom_table=nom_table
        self.conn=conn
    
    def insert_data(self, data):
        cur=self.conn.cursor()
        my_valeur=[]
        for i,elt in enumerate(data):
            my_valeur.append((data[i]["col_1"],data[i]["col_2"],data[i]["col_3"]))
        mes_lignes=tuple(my_valeur) 
        
        for ma_ligne in mes_lignes:
            cur.execute(f"INSERT INTO My_table VALUES {ma_ligne}")
        self.conn.commit()
  
    def read_data(self,nom_colonne,valeur): 
        cur=self.conn.cursor()
        cur.execute(f"SELECT * FROM {self.nom_table} WHERE {nom_colonne} = '{valeur}'")
        resultats = cur.fetchall()
        return resultats
    
    def update_data(self, nom_colonne, valeur_upd, update_data):
        """Modifier une valeur d'une colonne"""

        cur = self.conn.cursor()

        my_cols=""
        for col in update_data:
            my_cols= my_cols + col + "='" + str(update_data[col])+"',"
        my_cols=my_cols[0:-1]

        cur.execute(f"UPDATE {self.nom_table} SET {my_cols} WHERE {nom_colonne} = {valeur_upd}")

        self.conn.commit()
        
        
    def delete_data(self,nom_colonne,valeur):
        
        cur = self.conn.cursor()
        
        cur.execute(f"DELETE FROM {self.nom_table} WHERE {nom_colonne} = '{valeur}'")
        
        self.conn.commit()

class DataBase():
    def __init__(self, nom_db, type_db):
        self.nom_db=nom_db
        self.type_db=type_db
        
        self.conn = None
        if type_db=="sqlite":
            self.conn = sqlite3.connect(nom_db)
            print(f"connexion à la Db {nom_db} créée")
    
    def create_table(self, nom_table, dict_col): 
        my_cols=""
        for col in dict_col:
            my_cols=my_cols+ col +" "+dict_col[col]+","
        my_cols=my_cols[0:-1]
        cur = self.conn.cursor()
        cur.execute(f"DROP TABLE IF EXISTS {nom_table}")
        cur.execute(f"CREATE TABLE {nom_table} ({my_cols})")
        
        table=Table(self.conn,nom_table)
        return table